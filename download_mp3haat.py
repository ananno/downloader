import os
import urllib
import urllib.request
from urlextract import URLExtract


def get_song_url(root_url, download=None):
    mp3urls = []
    try:
        print("Looking URL : ", root_url)
        req = urllib.request.Request(root_url, headers={'user-Agent': "Mozilla Browser"})
        cont = urllib.request.urlopen(req).read().decode('utf-8')
        extractor = URLExtract()
        urls = extractor.find_urls(cont, only_unique=True)
        song_urls = [url for url in urls if "mp3.html" in url or ("mp3_" in url and ".html" in url)]
        count = 0
        for song_url in song_urls:
            print("Collecting mp3 files : ", song_url)
            req = urllib.request.Request(song_url, headers={'user-Agent': "Mozilla Browser"})
            cont = urllib.request.urlopen(req).read().decode('utf-8')
            mp3url = [url for url in extractor.find_urls(cont, only_unique=True) if url.endswith(".mp3")]
            if len(mp3url) > 0:
                url = mp3url[0]
                mp3urls.append(url)
                count += 1
                if download:
                    req = urllib.request.Request(url, headers={'user-Agent': "Mozilla Browser"})
                    cont = urllib.request.urlopen(req)
                    filename = os.path.join(download, os.path.basename(url).title())
                    print("Downloading {}: {}".format(url, filename))
                    with open(filename, "wb") as out:
                        out.write(cont.read())
        print("Done : {} mp3 files".format(count))
    except Exception as excp:
        print(excp)
    finally:
        return mp3urls


if __name__ == '__main__':
    mp3s = []
    base_paged_url = "http://www.mp3haat.com/tag/nachiketa/page/{}/"
    download_loc = "/home/hasibur/Music/Nachiketa"
    for i in range(1, 7):
        print("Total mp3s : ", len(mp3s))
        url = base_paged_url.format(i)
        mp3s += get_song_url(url, download_loc)
    print("Total mp3s : ", len(mp3s))
    print("Finish")

