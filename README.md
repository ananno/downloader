# Downloader

> **Disclaimer**
>
> These scripts downloads files which may be subjects to privacy violation or
> direct act of piracy. Author is not responsible of using the script what so ever.
> If you want to use this script the following consequence regarding any legal issue
> is sole responsibility of the user. This author will not be responsible of any such
> action. 


## Mp3 : mp3haat.com
* Script: `download_mp3haat.py`
This is a python script for downloading mp3 files from paged search pages. It will collect the mp3 file page urls,
 go to the individual pages and download the `mp3` file in designated location.